import { combineReducers } from 'redux';

import tasks from './tasks';
import filterTasks from './filterTasks';

export default combineReducers({
  tasks,
  filterTasks
});
