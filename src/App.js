import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { AddForm } from './AddForm.js';


const tasks = [
  {
    "id": 12564,
    "city": "Самара",
    "title": "Подключение провода от клеммной коробки пожарной сигнализации к расцепителю, проверить работоспособность",
    "data": "13 августа",
    "money": "10 021 Р",
  },
  {
    "id": 12564,
    "city": "Самара",
    "title": "Подключение провода от клеммной коробки пожарной сигнализации к расцепителю, проверить работоспособность",
    "data": "13 августа",
    "money": "10 021 Р",
  },
  {
    "id": 12564,
    "city": "Самара",
    "data": "13 августа",
    "title": "Подключение провода от клеммной коробки пожарной сигнализации к расцепителю, проверить работоспособность",
    "money": "10 021 Р",
  },
  {
    "id": 12564,
    "city": "Самара",
    "title": "Подключение провода от клеммной коробки пожарной сигнализации к расцепителю, проверить работоспособность",
    "data": "13 августа",
    "money": "10 021 Р",
  }
]

class App extends Component {
  constructor() {
   super()
   // initialize your options array on your state
   this.state = {
     tasks: [],
     isAdd: false,
     searchString: [],
     query:"",
   }

 }

findTask() {
  console.log('find track', this.searchInput.value)
  this.props.onFindTask(this.searchInput.value)
}



toggleAdd() {
  this.setState({ isAdd: !this.state.Add} );
}

handleTaskAdd (newTask) {
      let newTasks = this.state.tasks;
      newTasks.push(newTask);
      this.setState({ tasks: newTasks });
      this.setState({ isAdd: false });
}

  render() {
    return (
      <div className="App">
            <header >
              <div className="headers">
                <div className="header_left">
                <div className="symbol">taskon</div>
              <div className="header1">Задания </div>
                <div className="header1">Финансы </div>
              <div className="header1">Компания </div>
                <div className="header1">Статистика</div>
                </div>

                <div className="header_right">
                  <div>Balance</div>
                  <div>User</div>
                  <div>?</div>
                </div>

                </div>
            </header>
            <section className="recktangle">
            <div className="title"><h1>Задания</h1></div>
            <div className="vkladki">
              <div className="vkladka1">Новые</div>
              <div className="vkladka1">В работе</div>
              <div className="vkladka1">На рассмотрении</div>
              <div className="vkladka1">Выполнены</div>
              <div className="vkladka1">Отменены</div>
              <div className="vkladka1">Черновики</div>
            </div>
            <hr align="center" width="1150" size="1" color="#999999" />
<br />
           <button className="main_button" onClick={this.toggleAdd.bind(this)}>+</button>
          {this.state.isAdd && <AddForm onTaskAdd={this.handleTaskAdd} onAddCancel={this.handleAddCancel} />}
            <div className="querys">
            <input
              type="text"
              id="filter"
              placeholder="Поиск по слову"
              onChange={this.handleInputChange}
              onSubmit={this.findTask.bind(this)}
              ref={(input) => {this.searchInput = input} }
              />
            <div><input
              type="checkbox" id="cb1" /> <label for="cb1">Персональное</label></div>
              <div><input type="checkbox" id="cb1" /> <label for="cb1">Ночное</label></div>
              <div><input type="checkbox" id="cb1" /> <label for="cb1">Срочное</label></div>
            </div>

<br />
     <div className="titles">
     <th>ID</th>
     <div className="city"><th>Город</th> </div>
     <div className="title_"><th>Название</th></div>
     <div className="data"><th>Выполнить до</th></div>
     <div className="money"><th>Сумма</th></div>
     </div>
     <hr align="center" width="1150" size="1" color="#999999" />
     <br />
            <div>
              {tasks.map((task) =>
                <div className="items_titles">
                  <td>{task.id}</td>
                  <td>{task.city}</td>
                  <td className="item_title">{task.title}</td>
                  <td>{task.data}</td>
                  <td>{task.money}</td>
                </div>
              )}
           </div>
            </section>
      </div>
    );
  }

  _updateLocalStorage() {
      const tasks = JSON.parse(tasks); // приводим массив объектов в стейте к строке
      localStorage.setItem('tasks', tasks); //создаем в локал сторедже браузера объект, где первый аргумент это ключ, а второй наша переменная.
  }

}

export default App;
