import React from 'react';

export class AddForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: Date.now,
            category: " ",
            brand: " ",
            title: " ",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleTaskAdd = this.handleTaskAdd.bind(this);
    }

    componentDidMount() {
        document.body.classList.add('background-grey');
    }

    componentWillUnmount() {
        document.body.classList.remove('background-grey');
    }

    handleChange(e) {
        this.setState({
            [e.target.title]: e.target.value
        });
    }

    handleTaskAdd() {
        let newTask = {
          id: Date.now(),
          category: this.state.category,
          brand: this.state.brand,
          title: this.state.title,
        };
        this.props.onTaskAdd(newTask);
        this.setState({

          category: " ",
          brand: " ",
          title: " ",
        });
    }

    render() {
        return (
            <div className='form-container'>
                <form className='form'>
                    <h2>Добавте новое задание</h2>
                    <label className='form-label'>Id
                        <input className='form-input' name='id' type='text' onChange={this.handleChange} value={this.state.id}/>
                    </label>
                    <label className='form-label'>Category
                        <input className='form-input' name='category' type='text' onChange={this.handleChange} value={this.state.category}/>
                    </label>
                    <label className='form-label'>Brand
                        <input className='form-input' name='brand' type='text' onChange={this.handleChange} value={this.state.brand} />
                    </label>
                    <label className='form-label'>Title
                        <input className='form-input' name='title' type='url' onChange={this.handleChange} value={this.state.title}/>
                    </label>
                    <div className='form-btns'>
                        <button className='save-btn' type='button' onClick={this.handleTaskAdd}>Сохранить</button>
                        <button className='cancel-btn' onClick={this.props.onAddCancel}>Отменить</button>
                    </div>
                </form>
            </div>
        );
    }
}
